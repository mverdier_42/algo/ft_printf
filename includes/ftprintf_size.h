/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_search.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:48:32 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:50:54 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_SIZE_H
# define FTPRINTF_SIZE_H

# include "ftprintf_params.h"
# include "ftprintf_args.h"
# include "ftprintf_lens.h"
# include "libft.h"
# include "ftprintf_save_arg.h"
# include "ftprintf_colors.h"

# include <stdarg.h>
# include <string.h>
# include <wctype.h>
# include <inttypes.h>

int		calc_size(const char *format, t_params *params,
		va_list ap, t_args **args);
int		size_types(t_params *param, t_args **args, va_list ap);
int		size_flags_part1(t_params *param, t_args *tmp, int size);
int		size_flags_part2(t_params *param, t_args *tmp, int size);

int		calc_int(t_params *param, va_list ap, t_args **args);
int		calc_unsigned(t_params *param, va_list ap, t_args **args);
int		calc_bin(t_params *param, va_list ap, t_args **args);
int		calc_octal(t_params *param, va_list ap, t_args **args);
int		calc_hexa(t_params *param, va_list ap, t_args **args);

int		calc_char(t_params *param, va_list ap, t_args **args);
int		calc_string(t_params *param, va_list ap, t_args **args);
int		calc_ptr(t_params *param, va_list ap, t_args **args);
int		calc_wlen(wchar_t c);

#endif
