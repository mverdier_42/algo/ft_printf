/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_params.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:46:04 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:47:44 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_PARAMS_H
# define FTPRINTF_PARAMS_H

# include "libft.h"

# include <string.h>
# include <stdlib.h>

typedef struct	s_params
{
	char			type;
	char			size;
	int				prec;
	t_bool			is_prec;
	int				width;
	int				minus;
	int				plus;
	int				hash;
	int				zero;
	int				space;
	int				len;
	int				elem;
	struct s_params	*next;
}				t_params;

t_params		*push_back_new(t_params **params);
void			delete_params(t_params **params);
int				size_is_higher(t_params *param, const char *format, int i);

#endif
