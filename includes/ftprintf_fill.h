/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_fill.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:45:05 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:47:10 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_FILL_H
# define FTPRINTF_FILL_H

# include "ftprintf_args.h"
# include "ftprintf_params.h"
# include "libft.h"
# include "ftprintf_lens.h"
# include "ftprintf_size.h"

# include <stdlib.h>

int				fill_buf(const char *format, char *buf,
		t_params *params, t_args *args);

int				fill_flags(char *buf, int *j, t_args *arg, t_params *param);
void			fill_width(char *buf, int *j, t_params *param, t_args *arg);
void			fill_minus(char *buf, int *j, t_params *param, t_args *arg);
void			fill_zero(char *buf, int *j, t_params *param, t_args *arg);
void			fill_prec(char *buf, int *j, t_params *param, t_args *arg);
int				fill_types(char *buf, int *j, t_args *arg, t_params *param);

int				fill_wchar(char *buf, int *j, wint_t c);
int				fill_wstr(char *buf, int *j, wchar_t *s, t_params *param);
char			*fill_intmax_to_a(intmax_t n, char *str, int *i,
		t_params *param);
char			*fill_uintmax_to_a(uintmax_t n, unsigned int base, char *str,
		int *i);

#endif
