NAME = libftprintf.a

SRC = ft_printf.c ftprintf_search_flags.c ftprintf_search_params.c			\
	  ftprintf_params.c ftprintf_args.c ftprintf_calc_chars.c				\
	  ftprintf_calc_digits.c ftprintf_size.c ftprintf_lens_types.c			\
	  ftprintf_save_arg.c ftprintf_fill.c ftprintf_fill_itoa.c				\
	  ftprintf_fill_types.c ftprintf_fill_flags.c ftprintf_fill_width.c		\
	  ftprintf_args_types.c ftprintf_fill_wchars.c ftprintf_size_flags.c

LIBSRC = ft_memmove.c ft_strdup.c ft_strcpy.c ft_strlen.c

SRCDIR = ./srcs

LIBSRCDIR = ./libft/srcs

OBJ = $(SRC:%.c=$(OBJDIR)/%.o)

LIBOBJ = $(LIBSRC:%.c=$(LIBOBJDIR)/%.o)

OBJDIR = ./objs

LIBOBJDIR = ./libft/objs

INCDIR = ./includes

CC = gcc

CFLAGS = -Wall -Wextra -Werror

LFLAGS = -L ./libft -lft

INCFLAGS = -I ./includes -I ./libft/includes

all: $(NAME)

$(NAME): $(OBJ)
	@make -C libft all
	@ar -crs $@ $(OBJ) $(LIBOBJ)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) $(INCFLAGS) -o $@ -c $<

clean:
	@rm -rf $(OBJDIR)
	@make -C libft clean

fclean: clean
	@rm -rf $(NAME) test test2 wchar speed
	@make -C libft fclean

re: fclean
	@make -C libft fclean
	$(MAKE) all

norm:
	@echo "\033[1;32mPassage de la norminette :"
	@norminette $(SRC) $(INC)

printf:
	@echo "\033[1;32mVerifications des printf : \033[1;31m"
	@cat $(SRC) | grep printf | cat

check: no printf

git:
	@git add Makefile $(SRCDIR) $(INCDIR) auteur

test: $(OBJ)
	@$(CC) $(CFLAGS) -g main.c $^ $(INCFLAGS) $(LFLAGS) -o $@
	@./$@

test2: $(OBJ)
	@$(CC) main.c $^ $(INCFLAGS) -o $@
	@./$@

wchar: $(OBJ)
	@$(CC) test_wchar_main.c $^ $(INCFLAGS) -o $@
	@./$@

speed: $(OBJ)
	@$(CC) -g speedtest_ft_printf.c $^ $(INCFLAGS) -o $@
	@./$@

.PHONY: all clean re fclean git norm printf check test test2 valgrind speed
