/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:43:36 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/08 19:33:21 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

int			ft_intlen(int n)
{
	int size;

	size = 1;
	if (n < 0)
	{
		n = -n;
		size++;
	}
	while (n > 9)
	{
		n = n / 10;
		size++;
	}
	return (size);
}

static char		*ft_conv(int n, int *i, char *str)
{
	if (n <= 9)
	{
		str[*i] = n + 48;
		(*i)++;
	}
	if (n > 9)
	{
		str = ft_conv(n / 10, i, str);
		str[*i] = (n % 10) + 48;
		(*i)++;
	}
	return (str);
}

static char		*ft_intmin(char *str, int *n)
{
	int i;

	str[0] = '-';
	str[1] = '2';
	i = 2;
	*n = 147483648;
	return (str = ft_conv(*n, &i, str));
}

char			*ft_itoa(int n)
{
	char	*str;
	int		i;
	int		*ptr;

	if ((str = ft_strnew(ft_intlen(n))) == NULL)
		return (NULL);
	i = 0;
	if (n == -2147483648)
		return (str = ft_intmin(str, &n));
	if (n < 0)
	{
		n = -n;
		str[i] = '-';
		i++;
	}
	ptr = &i;
	if (n > 9)
		str = ft_conv(n, ptr, str);
	else
		str[i] = n + 48;
	return (str);
}
