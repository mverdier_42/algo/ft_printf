/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:49:55 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/07 12:41:25 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t	len;
	size_t	max;
	size_t	size2;

	len = ft_strlen(dest);
	max = len;
	size2 = 0;
	if (size > 1)
		size2 = size - 1;
	while (*src && max < size2)
	{
		dest[max] = *src;
		max++;
		src++;
	}
	dest[max] = '\0';
	if (size < len)
		return (size + ft_strlen(src));
	if (max == size - 1)
		return (size - 1 + ft_strlen(src));
	return (max);
}
