/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:51:09 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/07 16:33:06 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t i;

	if (!s1 || !s2)
		return (0);
	i = 0;
	if (s1 == s2)
		return (1);
	while (i < n)
	{
		if (((s1[i] && s2[i]) && (s1[i] >= 'a' && s1[i] <= 'z')
					&& (s1[i] == s2[i] || s1[i] == (s2[i] + 32))))
			i++;
		else if (((s1[i] && s2[i]) && (s1[i] >= 'A' && s1[i] <= 'Z')
					&& (s1[i] == s2[i] || s1[i] == (s2[i] - 32))))
			i++;
		else if (s1[i] == '\0' && s2[i] == '\0')
			return (1);
		else
			return (0);
	}
	return (1);
}
