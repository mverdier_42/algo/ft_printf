/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:49:13 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/07 16:32:20 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strequ(char const *s1, char const *s2)
{
	int i;

	if (!s1 || !s2)
		return (0);
	i = 0;
	if (s1 == s2)
		return (1);
	while (s1[i] && s2[i])
	{
		if (((s1[i] && s2[i]) && (s1[i] >= 'a' && s1[i] <= 'z')
					&& (s1[i] == s2[i])))
			i++;
		else if (((s1[i] && s2[i]) && (s1[i] >= 'A' && s1[i] <= 'Z')
					&& (s1[i] == s2[i])))
			i++;
		else
			return (0);
	}
	if (s1[i] || s2[i])
		return (0);
	return (1);
}
