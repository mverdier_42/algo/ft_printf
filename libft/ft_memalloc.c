/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:44:15 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/03 19:44:17 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

void	*ft_memalloc(size_t size)
{
	void	*new_mem;

	if ((new_mem = malloc(sizeof(*new_mem) * size)) == NULL)
		return (NULL);
	new_mem = ft_memset(new_mem, 0, size);
	return (new_mem);
}
