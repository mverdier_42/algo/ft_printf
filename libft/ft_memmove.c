/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:45:39 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/05 18:19:52 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned char	*d;
	unsigned char	*s;
	size_t			i;

	d = (unsigned char *)dest;
	s = (unsigned char *)src;
	i = 0;
	if (s >= d)
	{
		while (i < n)
		{
			*(d + i) = *(s + i);
			i++;
		}
	}
	else
	{
		while (n > 0)
		{
			*(d + (n - 1)) = *(s + (n - 1));
			n--;
		}
	}
	return (dest);
}
