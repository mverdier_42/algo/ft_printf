/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 16:00:37 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 16:00:44 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_params.h"

int				size_is_higher(t_params *param, const char *format, int i)
{
	if (format[i] == 'h' && format[i + 1] == 'h' && (param->size == 'a'
				|| param->size == 'h'))
		return (1);
	if (format[i] == 'l' && format[i + 1] == 'l' && (param->size == 'a'
				|| param->size == 'h' || param->size == 'H'
				|| param->size == 'h' || param->size == 'z'))
		return (1);
	if (format[i] == 'h' && param->size == 'a')
		return (1);
	if (format[i] == 'l' && (param->size == 'a' || param->size == 'h'
				|| param->size == 'H' || param->size == 'z'))
		return (1);
	if (format[i] == 'j')
		return (1);
	if (format[i] == 'z' && (param->size == 'a' || param->size == 'h'))
		return (1);
	return (0);
}

static t_params	*init_param(t_params *param)
{
	param->type = 'a';
	param->size = 'a';
	param->prec = 0;
	param->is_prec = FALSE;
	param->width = 0;
	param->minus = 0;
	param->plus = 0;
	param->hash = 0;
	param->zero = 0;
	param->space = 0;
	param->len = 0;
	param->next = NULL;
	param->elem = 0;
	return (param);
}

static t_params	*create_param(void)
{
	t_params	*new_param;

	if ((new_param = (t_params *)malloc(sizeof(t_params))) == NULL)
		return (NULL);
	return (init_param(new_param));
}

void			delete_params(t_params **params)
{
	t_params	*tmp;

	while (*params)
	{
		tmp = (*params)->next;
		free(*params);
		*params = tmp;
	}
}

t_params		*push_back_new(t_params **params)
{
	t_params	*tmp;
	t_params	*param;

	if ((param = create_param()) == NULL)
		return (NULL);
	if (*params == NULL)
		*params = param;
	else
	{
		tmp = *params;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = param;
	}
	return (param);
}
