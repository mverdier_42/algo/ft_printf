/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_args.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:57:47 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:57:56 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_args.h"

void			delete_args(t_args **args)
{
	t_args	*tmp;

	while (*args)
	{
		tmp = (*args)->next;
		free(*args);
		*args = tmp;
	}
}

static t_args	*init_arg(t_args *arg)
{
	arg->type = NUL;
	arg->stype = INT;
	arg->len = 0;
	arg->next = NULL;
	return (arg);
}

static t_args	*create_arg(void)
{
	t_args	*new_arg;

	if ((new_arg = (t_args *)malloc(sizeof(t_args))) == NULL)
		return (NULL);
	return (init_arg(new_arg));
}

t_args			*push_back_new_arg(t_args **args)
{
	t_args	*tmp;
	t_args	*arg;

	if ((arg = create_arg()) == NULL)
		return (NULL);
	if (*args == NULL)
		*args = arg;
	else
	{
		tmp = *args;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = arg;
	}
	return (arg);
}
