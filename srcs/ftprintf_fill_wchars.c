/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_fill_wchars.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:59:50 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:59:59 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_fill.h"

static void	fill_buf_wchar(char *buf, int *j, wchar_t c, int n)
{
	int		bits_len;

	bits_len = bit_len(c);
	while (n > 0)
	{
		buf[(*j) + n] = (c & 63) | 128;
		c >>= 6;
		n--;
	}
	if (bits_len <= 11)
		buf[*j] = 0xc0 | c;
	else if (bits_len <= 16)
		buf[*j] = 0xe0 | c;
	else
		buf[*j] = 0xf0 | c;
}

int			fill_wchar(char *buf, int *j, wint_t c)
{
	wchar_t			ch;
	int				bits_len;
	int				n;

	ch = c;
	if ((ch > 55296 && ch < 65536) || ch > 1114111 || ch < 0)
		return (-1);
	n = 0;
	bits_len = bit_len(ch);
	if (bits_len <= 7)
		buf[*j] = ch;
	else if (bits_len <= 11)
		fill_buf_wchar(buf, j, ch, n = 1);
	else if (bits_len <= 16)
		fill_buf_wchar(buf, j, ch, n = 2);
	else
		fill_buf_wchar(buf, j, ch, n = 3);
	*j += n + 1;
	return (1);
}

int			fill_wstr(char *buf, int *j, wchar_t *s, t_params *param)
{
	int		i;
	int		n;
	char	*null;

	i = 0;
	n = 0;
	null = ft_strdup("(null)");
	if (!s)
	{
		ft_memmove(buf + *j, null, *j += 6);
		return (1);
	}
	while (s[i] && ((n += calc_wlen(s[i])) <= param->prec
				|| param->is_prec == FALSE))
	{
		if ((fill_wchar(buf, j, s[i]) == -1))
		{
			free(null);
			return (-1);
		}
		i++;
	}
	free(null);
	return (1);
}
