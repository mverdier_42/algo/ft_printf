/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_calc_chars.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:58:21 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:58:28 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_size.h"

int			calc_wlen(wchar_t c)
{
	int		bits_len;

	bits_len = bit_len(c);
	if (bits_len <= 7)
		return (1);
	else if (bits_len <= 11)
		return (2);
	else if (bits_len <= 16)
		return (3);
	else
		return (4);
}

int			calc_char(t_params *param, va_list ap, t_args **args)
{
	t_args	*arg;

	arg = push_back_new_arg(args);
	arg->len = 1;
	if (param->type == 'C' || (param->type == 'c' && param->size == 'l'))
	{
		arg->arg.wi_t = va_arg(ap, wint_t);
		arg->type = WINT_T;
		return (calc_wlen(arg->arg.wi_t));
	}
	else if (param->type == 'c')
		arg->arg.c = va_arg(ap, int);
	else if (param->type == '%')
		arg->arg.c = param->type;
	arg->type = CHAR;
	return (1);
}

static int	calc_wstring(t_params *param, t_args *arg, va_list ap)
{
	int		len;
	int		tmp;
	int		i;

	len = 0;
	i = 0;
	arg->type = WCHAR_T;
	if ((arg->arg.wc_t = va_arg(ap, wchar_t*)) == NULL)
		return (6);
	while ((arg->arg.wc_t)[i] && ((tmp = len) < param->prec
				|| param->is_prec == FALSE))
	{
		if ((len += calc_wlen((arg->arg.wc_t)[i++])) > param->prec
				&& param->is_prec == TRUE)
			return (tmp);
	}
	return (len);
}

int			calc_string(t_params *param, va_list ap, t_args **args)
{
	t_args	*arg;
	int		len;

	arg = push_back_new_arg(args);
	len = 0;
	if (param->type == 'S' || (param->type == 's' && param->size == 'l'))
		len = calc_wstring(param, arg, ap);
	else if (param->type == 's' && param->size == 'a')
	{
		arg->type = STR;
		len = ft_strlen(save_char_arg(arg, ap));
		if (param->is_prec == TRUE)
			len = (param->prec < len) ? param->prec : len;
		arg->len = len;
	}
	return (len);
}

int			calc_ptr(t_params *param, va_list ap, t_args **args)
{
	t_args	*arg;

	arg = push_back_new_arg(args);
	if (param->type == 'p')
		arg->type = UINT;
	return (arg->len =
			(ft_uintmax_base_len(save_uintmax_arg(arg, ap, param), 16)));
}
