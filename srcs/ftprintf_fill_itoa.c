/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_fill_itoa.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:59:22 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:59:30 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_fill.h"

static char	*ft_conv_imax(intmax_t n, int *i, char *str)
{
	if (n <= 9)
	{
		str[*i] = n + 48;
		(*i)++;
	}
	if (n > 9)
	{
		str = ft_conv_imax(n / 10, i, str);
		str[*i] = (n % 10) + 48;
		(*i)++;
	}
	return (str);
}

char		*fill_intmax_to_a(intmax_t n, char *str, int *i, t_params *param)
{
	if (n < 0)
	{
		if (param->plus == 0 && param->zero == 0 && param->space == 0 &&
				param->is_prec == FALSE)
			str[(*i)++] = '-';
		if (n == (intmax_t)-9223372036854775808ULL)
		{
			n = -223372036854775808;
			str[(*i)++] = '9';
		}
		n = -n;
	}
	if (n > 9)
		str = ft_conv_imax(n, i, str);
	else
		str[(*i)++] = n + 48;
	return (str);
}

static char	*ft_conv_uimax(uintmax_t n, int *i, char *str, unsigned int base)
{
	unsigned int	tmp;
	char			base_str[17];

	tmp = base;
	if (tmp < 17)
		ft_strcpy(base_str, "0123456789abcdef");
	else if (tmp == 17)
	{
		tmp = 16;
		ft_strcpy(base_str, "0123456789ABCDEF");
	}
	if (n < tmp)
	{
		str[*i] = (base_str)[n];
		(*i)++;
	}
	if (n >= tmp)
	{
		str = ft_conv_uimax(n / tmp, i, str, base);
		str[*i] = (base_str)[n % tmp];
		(*i)++;
	}
	return (str);
}

char		*fill_uintmax_to_a(uintmax_t nb, unsigned int base, char *str,
		int *i)
{
	unsigned int	tmp;
	char			base_str[17];

	tmp = base;
	if (tmp == 17)
	{
		tmp = 16;
		ft_strcpy(base_str, "0123456789ABCDEF");
	}
	else
		ft_strcpy(base_str, "0123456789abcdef");
	if (nb >= tmp)
		str = ft_conv_uimax(nb, i, str, base);
	else
		str[(*i)++] = (base_str)[nb];
	return (str);
}
