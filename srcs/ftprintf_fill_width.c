/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_fill_width.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 16:00:05 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 16:00:13 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_fill.h"

void		fill_prec(char *buf, int *j, t_params *param, t_args *arg)
{
	if ((param->space || param->plus) && !arg_is_positive(arg))
		(param->len)--;
	else if (arg_is_positive(arg) == 0)
	{
		buf[(*j)++] = '-';
		(param->len)--;
	}
	if (param->type == 'p')
		param->len -= 2;
	while (param->prec - param->len > 0)
	{
		buf[(*j)++] = '0';
		(param->len)++;
	}
}

void		fill_zero(char *buf, int *j, t_params *param, t_args *arg)
{
	if (!arg_is_positive(arg))
	{
		(param->len)++;
		if (param->plus == 0 && param->space == 0)
		{
			buf[(*j)++] = '-';
			(param->len)--;
		}
	}
	else if (arg_is_positive(arg) && (param->plus || param->space)
			&& arg_is_int(arg))
		(param->len)++;
	while (param->width - param->len > 0)
	{
		buf[(*j)++] = '0';
		(param->width)--;
	}
}

void		fill_minus(char *buf, int *j, t_params *param, t_args *arg)
{
	fill_width(buf, j, param, arg);
}

static int	calc_len_width(t_params *param, t_args *arg, int len)
{
	len = (param->prec > param->len) ? param->prec : param->len;
	if ((param->space || param->plus) && arg_is_positive(arg)
			&& arg_is_int(arg))
	{
		len = (ft_intmax_len(arg->arg.i) > len)
			? ft_intmax_len(arg->arg.i) : len;
		len++;
	}
	if (param->plus && !arg_is_positive(arg))
		len++;
	if (param->type == 'p' && arg_is_zero(arg) && param->is_prec == TRUE
			&& param->prec == 0)
		len--;
	return (len);
}

void		fill_width(char *buf, int *j, t_params *param, t_args *arg)
{
	int len;

	if (!arg_is_char(arg))
		len = calc_len_width(param, arg, 0);
	else
		len = param->len;
	if (!arg_is_positive(arg) && param->is_prec)
		len++;
	if (param->prec < param->len && param->is_prec && !arg_is_positive(arg))
		len--;
	while (param->width - len > 0)
	{
		buf[(*j)++] = ' ';
		(param->width)--;
	}
}
