/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_fill_flags.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:59:06 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:59:14 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_fill.h"

static void	fill_space(char *buf, int *j, t_params *param, t_args *arg)
{
	if (arg_is_int(arg))
	{
		if (param->plus == 1 && arg_is_positive(arg))
			buf[(*j)++] = '+';
		else if (param->plus == 1 && !arg_is_positive(arg))
			buf[(*j)++] = '-';
		else if (param->space == 1 && arg_is_positive(arg))
			buf[(*j)++] = ' ';
		else if (param->space == 1 && !arg_is_positive(arg))
			buf[(*j)++] = '-';
	}
}

static void	fill_hash(char *buf, int *j, t_params *param, t_args *arg)
{
	if (!(buf + *j))
		return ;
	if ((param->type == 'o' || param->type == 'O') && arg_is_zero(arg) == 0)
		buf[(*j)++] = '0';
	else if ((param->type == 'x' && arg->arg.ui != 0) || param->type == 'p')
		ft_memmove(buf + *j, "0x", *j += 2);
	else if (param->type == 'X' && arg_is_zero(arg) == 0)
		ft_memmove(buf + *j, "0X", *j += 2);
}

int			fill_flags(char *buf, int *j, t_args *arg, t_params *param)
{
	if (param->type == 0)
		return (1);
	if ((param->zero == 0 || (param->is_prec && !arg_is_char(arg)))
			&& param->minus == 0 && param->width > 0)
		fill_width(buf, j, param, arg);
	if (param->hash == 1)
		fill_hash(buf, j, param, arg);
	if (param->plus == 1 || param->space == 1)
		fill_space(buf, j, param, arg);
	if (param->prec >= 0 && param->is_prec && !arg_is_char(arg))
		fill_prec(buf, j, param, arg);
	if (param->zero == 1 && param->minus == 0 && (!param->is_prec
				|| arg_is_char(arg)))
		fill_zero(buf, j, param, arg);
	if ((fill_types(buf, j, arg, param) == -1))
		return (-1);
	if (param->minus == 1)
		fill_minus(buf, j, param, arg);
	return (1);
}
